package com.example.quiz

import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.quiz.databinding.FragmentFinalScoreBinding
import com.example.quiz.databinding.FragmentQuizQuestionsBinding

class QuizQuestionsFragment : Fragment(), View.OnClickListener{
    private var wasAlreadySelected = false
    private var score : Int = 0
    private val answers = mutableListOf<Int>()
    private var mCurrentPosition: Int = 1
    private lateinit var mQuestionList: List<Question>
    private var mSelectedOptionPosition: Int = 0
    private var _binding: FragmentQuizQuestionsBinding? = null
    private val binding : FragmentQuizQuestionsBinding
        get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentQuizQuestionsBinding.inflate(layoutInflater)

        binding.tvOptionOne.setOnClickListener(this)
        binding.tvOptionTwo.setOnClickListener(this)
        binding.tvOptionThree.setOnClickListener(this)
        binding.tvOptionFour.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mQuestionList = Constants.getQuestions()

        restart()
    }

    private fun setQuestion() {
        val question = mQuestionList[mCurrentPosition - 1]

        defaultOptionsView()
        if(mCurrentPosition == mQuestionList.size){
            binding.btnSubmit.text = requireContext().getString(R.string.end)
        }
        else{
            binding.btnSubmit.text = requireContext().getString(R.string.confirm)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.progressBar.setProgress(mCurrentPosition,true)
        }else{
            binding.progressBar.progress = mCurrentPosition
        }
        binding.tvProgress.text = String.format(requireContext().getString(R.string.progress),mCurrentPosition,binding.progressBar.max)

        binding.tvQuestion.text = question.question

        binding.tvOptionOne.text = question.optionOne
        binding.tvOptionTwo.text = question.optionTwo
        binding.tvOptionThree.text = question.optionThree
        binding.tvOptionFour.text = question.optionFour
    }

    private fun defaultOptionsView() {
        val options = ArrayList<TextView>()
        options.add(0, binding.tvOptionOne)
        options.add(1, binding.tvOptionTwo)
        options.add(2, binding.tvOptionThree)
        options.add(3, binding.tvOptionFour)

        for (option in options) {
            option.setTextColor(Color.parseColor("#7A8089"))
            option.typeface = Typeface.DEFAULT
            option.background = ContextCompat.getDrawable(requireContext(), R.drawable.default_option_border_bg)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_option_one -> {
                if(!wasAlreadySelected)
                    selectedOptionView(binding.tvOptionOne, 1)
            }
            R.id.tv_option_two -> {
                if(!wasAlreadySelected)
                    selectedOptionView(binding.tvOptionTwo, 2)
            }
            R.id.tv_option_three -> {
                if(!wasAlreadySelected)
                    selectedOptionView(binding.tvOptionThree, 3)
            }
            R.id.tv_option_four -> {
                if(!wasAlreadySelected)
                    selectedOptionView(binding.tvOptionFour, 4)
            }
            R.id.btn_submit -> {
                if (mSelectedOptionPosition == 0) {
                    if(!wasAlreadySelected) return
                    wasAlreadySelected = false
                    mCurrentPosition++
                    when {
                        mCurrentPosition <= mQuestionList.size -> {
                            setQuestion()
                        }
                        else -> {
                            showScore()
                            wasAlreadySelected = true
                        }
                    }
                } else {
                    wasAlreadySelected = true
                    val question = mQuestionList[mCurrentPosition - 1]
                    answers += mSelectedOptionPosition
                    // This is to check if the answer is wrong
                    if (question.correctAnswer != mSelectedOptionPosition) {
                        answerView(mSelectedOptionPosition, R.drawable.wrong_option_border_bg)
                    }else{
                        score++
                    }

                    // This is for correct answer
                    answerView(question.correctAnswer, R.drawable.correct_option_border_bg)

                    if (mCurrentPosition == mQuestionList.size) {
                        binding.btnSubmit.text = requireContext().getString(R.string.end)
                    } else {
                        binding.btnSubmit.text = requireContext().getString(R.string.next_question)
                    }

                    mSelectedOptionPosition = 0

                }
            }
        }
    }

    private fun showScore(){
        val argument = IntArray(answers.size){1}
        for(i in answers.indices)
            argument[i] = answers[i]
        val action = QuizQuestionsFragmentDirections.actionQuizQuestionsFragmentToFinalScoreFragment(score,argument)
        findNavController().navigate(action)
    }
    private fun answerView(answer: Int, drawableView: Int) {
        when (answer) {
            1 -> {
                binding.tvOptionOne.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            2 -> {
                binding.tvOptionTwo.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            3 -> {
                binding.tvOptionThree.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            4 -> {
                binding.tvOptionFour.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
        }
    }
    private fun restart(){
        wasAlreadySelected = false
        mCurrentPosition = 1
        score = 0
        answers.clear()
        setQuestion()
    }
    private fun selectedOptionView(tv: TextView, selectedOptionNum: Int) {

        defaultOptionsView()
        mSelectedOptionPosition = selectedOptionNum

        tv.setTextColor(Color.parseColor("#363A43"))
        tv.setTypeface(tv.typeface, Typeface.BOLD)
        tv.background = ContextCompat.getDrawable(requireContext(), R.drawable.selected_option_border_bg)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}