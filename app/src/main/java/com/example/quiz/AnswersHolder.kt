package com.example.quiz

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz.databinding.AnswersRowLayoutBinding

class AnswersHolder(
    private val binding : AnswersRowLayoutBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(question: Question,picked : Int){

        binding.apply {
            tvOptionOne.text = question.optionOne
            tvOptionOne.background = ContextCompat.getDrawable(requireContext(),R.drawable.default_option_border_bg)
            tvOptionTwo.text = question.optionTwo
            tvOptionTwo.background = ContextCompat.getDrawable(requireContext(),R.drawable.default_option_border_bg)
            tvOptionThree.text = question.optionThree
            tvOptionThree.background = ContextCompat.getDrawable(requireContext(),R.drawable.default_option_border_bg)
            tvOptionFour.text = question.optionFour
            tvOptionFour.background = ContextCompat.getDrawable(requireContext(),R.drawable.default_option_border_bg)
            questionTextView.text = question.question
        }
        if(picked == question.correctAnswer){
            answerView(picked,R.drawable.correct_option_border_bg)
        }else{
            answerView(picked,R.drawable.wrong_option_border_bg)
            answerView(question.correctAnswer,R.drawable.correct_option_border_bg)
        }
    }
    private fun requireContext() = binding.root.context
    private fun answerView(answer: Int, drawableView: Int) {
        when (answer) {
            1 -> {
                binding.tvOptionOne.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            2 -> {
                binding.tvOptionTwo.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            3 -> {
                binding.tvOptionThree.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
            4 -> {
                binding.tvOptionFour.background = ContextCompat.getDrawable(requireContext(), drawableView)
            }
        }
    }
}