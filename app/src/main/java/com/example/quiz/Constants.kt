package com.example.quiz

object Constants {

    fun getQuestions(): List<Question>{
        val questionsList = mutableListOf<Question>()

        val que1 = Question(1, "Owoc z meszkiem to",
            "Morela",
            "Winogrono"
            ,"Gruszka",
            "Czereśnia",
            1)

        questionsList.add(que1)

        val que2 = Question(2, "Zielony będzie koktajl",
            "Z grejpgruta z odrobiną liczi",
            "Z banana z cząstką limonki"
            ,"Z jarmużu z krztą ananasa",
            "Z jagód z kawałkiem imbiru",
            3)

        questionsList.add(que2)

        val que3 = Question(3, "Który owoc kryje w sobie więcej niż jedno nosiono",
            "Gruszka",
            "Liczi"
            ,"Awokado",
            "Orzech włoski",
            1)

        questionsList.add(que3)

        val que4 = Question(4, "Jakie ogórki są podstawą tradycyjnej zupy ogórkowej?",
            "Korniszony",
            "Pikle"
            ,"Kiszone",
            "Konserwowe",
            3)

        questionsList.add(que4)

        val que5 = Question(5, "Z czym najłatwiej pomylić pasternak?",
            "Z pietruszką",
            "Ze szczwiem"
            ,"Z burakiem",
            "Z kalarepą",
            1)

        questionsList.add(que5)

        val que6 = Question(6, "Żulienka to warzywa:",
            "Zmielone na miałko",
            "Ogolone na zapałkę"
            ,"Pokrojone w zapałkę",
            "Pokrojone w grubą kostkę",
            3)

        questionsList.add(que6)

        val que7 = Question(7, "Słynne gumowate łakocie produkowane w Niemczech od 1922r. mają postać:",
            "Ptysi",
            "Misiów"
            ,"Rysi",
            "Chłoptysiów",
            2)

        questionsList.add(que7)

        val que8 = Question(8, "Gdy zapali się nam tłuszcz na patelni, powinniśmy:",
            "Przykryć ją pokrywką",
            "Zalać wodą"
            ,"Dolać więcej tłuszczu",
            "Próbować zdmuchnąć ogień",
            1)

        questionsList.add(que8)

        val que9 = Question(9, "Co podzielimy na dzwonka?",
            "Jarmuż",
            "Karczochy"
            ,"Uliki",
            "Kajzerkę",
            3)

        questionsList.add(que9)

        val que10 = Question(10, "Ich spłaszczona odmiana nazywana jest UFO:",
            "Arbuzów",
            "Jabłek"
            ,"Brzoskwiń",
            "Cytryn",
            3)

        questionsList.add(que10)

        val que11 = Question(11, "Profiterolki:",
            "Mają trzy kólka i hamulec",
            "Nawijamy na szpule"
            ,"Tną bobiny papieru",
            "Nadziewamy bitą śmietaną",
            4)

        questionsList.add(que11)

        val que12 = Question(12, "Fermentacja alkoholowa przeprowadzana przez drożdże nie jest " +
                "wykorzystywana do produkcji:",
            "Napojów alkoholowych",
            "Biopaliwa"
            ,"Pieczywa drożdżowego",
            "Antracytu",
            4)

        questionsList.add(que12)

        val que13 = Question(13, "Które z łakoci przypominają wyglądem i w dotyku wędkarskie" +
                "sztuczne przynęty zwane twisterami:",
            "Kolorowe żelki",
            "Tabliczki czekolady"
            ,"Ryrki z kremem",
            "Czekoladowe groszki",
            1)

        questionsList.add(que13)

        val que14 = Question(14, "Nie łuska się ze strąków",
            "Gryki",
            "Ciecierzycy"
            ,"Bobu",
            "Fasoli",
            1)

        questionsList.add(que14)

        val que15 = Question(15, "Co stanowi śmiertelnie niebezpieczną mieszankę?",
            "Pomelo z żubrówką",
            "Winogrona z piwem"
            ,"Liczi z szampanem",
            "Durian z żytniówką",
            4)

        questionsList.add(que15)

        return questionsList
    }

}