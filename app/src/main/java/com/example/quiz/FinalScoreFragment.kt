package com.example.quiz

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz.databinding.FragmentFinalScoreBinding

class FinalScoreFragment : Fragment() {
    private val args by navArgs<FinalScoreFragmentArgs>()
    private var _binding : FragmentFinalScoreBinding? = null
    private val mAdapter by lazy { AnswersAdapter(args.answers.asList()) }
    private val binding : FragmentFinalScoreBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFinalScoreBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.scoreTextView.text = String.format(requireContext().getString(R.string.score),args.score,15)
        binding.goBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
        setupAdapter()
    }
    private fun setupAdapter(){
        binding.recyclerview.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}