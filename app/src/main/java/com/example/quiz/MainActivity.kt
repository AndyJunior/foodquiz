package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import com.example.quiz.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN


        binding.btnStart.setOnClickListener {
            // if edit text is empty
            if(binding.etName.text.toString().isEmpty()) {
                Toast.makeText(this, "Podaj Imie!", Toast.LENGTH_SHORT).show()

            }
            else {
                // start questions activity
                val intent = Intent(this, QuizQuestionsActivity::class.java)
                startActivity(intent)
                // finish current activity
                finish()
            }
        }
    }
}