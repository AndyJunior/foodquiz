package com.example.quiz

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz.databinding.AnswersRowLayoutBinding

class AnswersAdapter(
    private val answers : List<Int>
): RecyclerView.Adapter<AnswersHolder>() {
    private val questions = Constants.getQuestions()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswersHolder {
        val binding = AnswersRowLayoutBinding.inflate(LayoutInflater.from(parent.context))
        return AnswersHolder(binding)
    }

    override fun onBindViewHolder(holder: AnswersHolder, position: Int) {
        holder.bind(question = questions[position],picked = answers[position])
    }

    override fun getItemCount(): Int = answers.count()
}